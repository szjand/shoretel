select *
FROM extrecordings
WHERE agent_number IN ('6190','6191','6192','6193')

-- this exposes morgans test calls FROM 8/15/18
SELECT *
FROM extcall a
WHERE a.calltype = 2
  AND CAST(a.starttime AS sql_date) = '08/15/2018'
  AND a.dialedNumberRight7 IN ('7576190','7576191','7576192','7576193')
  
  
      LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
        AND a.extension = b.extension
        AND LEFT(trim(a.sipCallId), 15) = b.guid  
		
SELECT top 10 *
FROM callCopyRecordings		
WHERE thedate = '08/15/2018'


-- calls FROM google numbers with recordings
SELECT a.*, b.*
FROM extcall a
LEFT JOIN callCopyRecordings b on LEFT(trim(a.sipCallId), 15) = b.guid
WHERE a.calltype = 2
  AND CAST(a.starttime AS sql_date) > curdate() -20
  AND a.dialedNumberRight7 IN ('7576190','7576191','7576192','7576193')
  AND b.guid IS NOT null
  
-- existing recordings FROM 08/15  
SELECT a.*, b.*
FROM extcall a
inner JOIN callCopyRecordings b on LEFT(trim(a.sipCallId), 15) = b.guid
  AND b.thedate = '08/15/2018'
  and CAST(a.startTime AS sql_date) = b.theDate


SELECT *
FROM extcall a
LEFT JOIN callCopyRecordings b on LEFT(trim(a.sipCallId), 15) = b.guid
WHERE cast(starttime AS sql_date) BETWEEN '03/28/2018' AND '03/29/2018'

  AND calltype = 3
  AND dialednumber like '%2071050'
  
/*  
any incoming call to ben knudson on 3/26 ~ 4:37 PM  

SELECT *
FROM extconnect
WHERE portname LIKE '%knudson%'
  AND CAST(connecttime AS sql_date) = '03/26/2018'
ORDER BY connecttime  


SELECT *
FROM extcall a
LEFT JOIN callCopyRecordings b on LEFT(trim(a.sipCallId), 15) = b.guid
WHERE a.id IN(7410371,7410815,7410947)
  AND a.calltype = 2
  
  
E:\Recordings\20180326\5847\5847-16-34-37.wav  
*/


-- calls FROM google numbers with recordings
SELECT cast(a.starttime AS sql_date) AS the_date, a.starttime, a.durationseconds, a.dialednumber, a.callerid, b.filename
-- SELECT a.*
FROM extcall a
LEFT JOIN callCopyRecordings b on LEFT(trim(a.sipCallId), 15) = b.guid
WHERE a.calltype = 2
  AND CAST(a.starttime AS sql_date) > '08/31/2018'
  AND a.dialedNumberRight7 IN ('7576190','7576191','7576192','7576193')

  
SELECT *
FROM extcall a
WHERE a.calltype = 2
  AND CAST(a.starttime AS sql_date) > '09/15/2018'
  AND a.dialedNumberRight7 IN ('7576190','7576191','7576192','7576193')  