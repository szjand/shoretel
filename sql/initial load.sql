after loading 50000 ids INTO extcall, AND corresponding rows (WHERE calltableid = call.id) 
FROM connect

SELECT *
FROM extconnect a
WHERE NOT EXISTS (
  SELECT 1
  FROM extcall
  WHERE id = a.calltableid)
  
  
SELECT *
FROM extcall a
WHERE NOT EXISTS (
  SELECT 1
  FROM extconnect
  WHERE calltableid = a.id)  
  
SELECT *
FROM extcall a
LEFT JOIN extconnect b on a.id = b.calltableid
WHERE b.id IS NULL   

SELECT *
FROM extconnect a
LEFT JOIN extcall b on a.calltableid = b.id
WHERE b.id IS NULL 
  
  
looks good AS a synch point  

select max(id) as startID from extCall

DELETE FROM extcall WHERE id = 1621874;
DELETE FROM extconnect WHERE calltableid = 1671904;

SELECT MAX(id) FROM extcall
-- so, i load the new calls, no need to list the new connect rows,
-- what are they?
-- this only works IF the 2 tables are initially synched
SELECT MIN(id)
FROM extcall a
WHERE NOT EXISTS (
  SELECT 1
  FROM extconnect
  WHERE calltableid = a.id)  
  
  
SELECT max(calltableid)
FROM extconnect a
WHERE EXISTS (
  SELECT 1
  FROM extcall
  WHERE id = a.calltableid)  
  
SELECT * FROM extcall ORDER BY starttime, id 

SELECT COUNT(*) FROM extcall -- 50013
SELECT COUNT(*) FROM extconnect -- 126809

delete FROM extcall WHERE CAST(starttime AS sql_date) = curdate();

delete FROM extconnect WHERE CAST(connecttime AS sql_date) = curdate();


-- 4/28, after running UPDATE, ending up with rows IN extconnect WHERE calltableid
-- does NOT exist IN extcall
maybe just trying to be too fancy IN setting the connect start point
bottom line, extCall IS the master, extConnect IS the detail

problem remains, AS long AS i am basing the scrape of connect on a calltableid
value being greater than some value, i am going to CONTINUE to get rows IN 
extConnect without corresponding rows IN extCall

hmmm, DO a BETWEEN 

select max(id) as startID from extCall

SELECT * FROM extConnect WHERE calltableid BETWEEN 1671930 AND 1675330

aha, NOT BETWEEN but calltableid > startid AND calltableid <= endID
seems to WORK
BETWEEN was inclusive of startId, those rows already exist IN connect

select max(id) as startID from extCall

SELECT COUNT(*) FROM extcall
UNION 
SELECT COUNT(*) FROM extconnect

-- uh oh, have i neglected that there may be gaps IN sequence, such that USING MAX AS a start point 
-- will exclude calls?

fuck, of course there are gaps
SELECT MIN(id), MAX(id) FROM extCall;

CREATE TABLE zjon (field1 integer);

DECLARE @i integer;
DECLARE @j integer;
DECLARE @m integer;
DECLARE @start integer;
DECLARE @END integer;
@i = (SELECT MIN(id) FROM extCall);
@j = (SELECT MAX(id) FROM extCall);
@m = @i;
WHILE @m <= @j DO
  INSERT INTO zjon values(@m);
  @m = @m + 1;
END WHILE;  

fuck, of course there are gaps
but, none of these call.id rows exist IN mysql either, yea!
SELECT *
FROM zjon a
WHERE NOT EXISTS (
  SELECT 1
  FROM extCall
  WHERE id = a.field1)

SELECT trim(CAST(field1 AS sql_char)) + ','
FROM zjon a
WHERE NOT EXISTS (
  SELECT 1
  FROM extCall
  WHERE id = a.field1)
  
  
SELECT * FROM extcall WHERE id IN ( 
SELECT calltableid FROM extconnect WHERE partyidlastname = 'bursinger') ORDER BY id DESC


SELECT * FROM extconnect WHERE partyidlastname = 'bursinger' ORDER BY id DESC  


SELECT a.id, d.name AS CallType, e.name AS ConnReason, f.name as DisConnReason, 
  cast(a.starttime AS sql_time) AS startTime, 
  cast(a.endtime AS sql_time) AS endTime, 
  a.durationseconds, 
  cast(b.connecttime AS sql_time) AS connTime, 
  cast(b.disconnecttime AS sql_time) AS DisConnTime,
  b.talktimeseconds, b.holdtimeseconds, b.ringtimeseconds, b.durationseconds,
  b.portName, b.groupname, c.name, b.partyid, b.partyidname
-- SELECT b.*  
FROM extcall a
LEFT JOIN extconnect b on a.id = b.calltableid
LEFT JOIN extpartyidflag c on b.partyidflags = c.partyidflag
LEFT JOIN extcalltype d on a.calltype = d.calltype
LEFT JOIN extconnectreason e on b.connectreason = e.connectreason
LEFT JOIN extDisconnectReason f on b.disconnectReason = f.disconnectReason
WHERE CAST(a.starttime AS sql_date) = curdate() - 1 
AND a.extension = '5914'
AND length(TRIM(a.dialednumber)) > 4
ORDER BY a.starttime, b.connecttime, b.disconnecttime


select starttime, 
  trim(cast(extract(hour from starttime) AS sql_char)) + ':' +
  trim(cast(extract(minute from starttime) AS sql_char)) + ':' +
  trim(cast(extract(second from starttime) AS sql_char)),
  CAST(starttime AS sql_time)
FROM extcall

-- 5/1, trying to make sense of of it ALL
-- which instance IS the "real" call
-- how to flatten the joined rows
--
-- best shot at sequence of events: sort BY call.id, call.starttime, connect.disconnecttime

SELECT callerid, COUNT(*) FROM extcall GROUP BY callerid ORDER BY COUNT(*) DESC 

Call TABLE:
  Extension:
    Outbound: For an outbound or extension-to-extension call, the dialed
      number of the originator of the call.
      This field is blank for an outbound call from an anonymous phone with no
      currently assigned DN.
    Inbound: The DN of the last party  involved in the call (excluding voice 
      mail or auto-attendant). For example, an
      incoming call to an extension that transferred the call to extension 300 has
      �300� in the extension field (the complete history of parties connecting to the
      call is in the Connect table).
      All calls to an extension that are forwarded to voice mail have the extension of
      the called party and not the voice mail number (15 characters, 0-length). 
      
Connect TABLE:      
  PartyType: party that initiated the call

select a.id, d.name as CallType, cast(a.starttime AS sql_time) AS StartTime, 
  cast(a.endtime AS sql_time) AS EndTime, a.durationSeconds, 
  a.extension, a.dialednumber, a.callerid,
  cast(b.connecttime AS sql_time) AS connTime, 
  cast(b.disconnecttime AS sql_time) AS DisConnTime,
  b.talktimeseconds, b.holdtimeseconds, b.ringtimeseconds, b.durationseconds,
  e.name AS ConnReason, f.name AS DisConnReason, g.name AS PartyType,
  b.portname, 
  b.groupid, b.groupname   
FROM extcall a
LEFT JOIN extconnect b on a.id = b.calltableid
LEFT JOIN extpartyidflag c on b.partyidflags = c.partyidflag
LEFT JOIN extcalltype d on a.calltype = d.calltype
LEFT JOIN extconnectreason e on b.connectreason = e.connectreason
LEFT JOIN extDisconnectReason f on b.disconnectReason = f.disconnectReason
LEFT JOIN extPartyType g on b.partytype = g.partytype
WHERE CAST(a.starttime AS sql_date) = curdate()
  AND a.id = 1686335
ORDER BY a.id, a.starttime, b.disconnecttime

SELECT a.id, COUNT(*)
FROM extCall a
LEFT JOIN extConnect b on a.id = b.calltableid
WHERE CAST(a.starttime AS sql_date) = curdate()
GROUP BY a.id
ORDER BY COUNT(*) desc