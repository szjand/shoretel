unit shoretelCallsDM;
(*)
4/28/14
  delphi to ads choked on datetime where year = 1601
  fix: AdsQuery.ParamByName('endTime').AsString := GetADSSqlTimeStamp(AdoQuery.FieldByName('endTime').AsDateTime);
  made startID module level var
6/22/14
  add field dialedNumber10 to extCall
(**)
interface

uses
  SysUtils, Classes, adsdata, adsfunc, adstable, adscnnct, DB, ADODB, DateUtils,
  IdSMTPBase, IdSMTP, IdMessage,
  Dialogs;

type
  TDM = class(TDataModule)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
    procedure PrepareQuery(query: TDataSet; sql: string='');
    procedure CloseQuery(query: TDataSet);
    function FormatWordToStr(const inVal: Word;
      const OutLength: Integer; const PadChar: Char = #48): String;
    function GetADSSqlTimeStamp(const DateTime: TDateTime): String;
    procedure OpenQuery(query: TDataSet; sql: string);
    procedure ExecuteQuery(query: TDataSet; sql: string);
    procedure curLoadPrevLoad(tableName: string);
    procedure curLoadIsEmpty(tableName: string);
    procedure SendMail(Subject: string; Body: string='');
    procedure ScrapeCountTable(db2Table, adsTable: string);
    procedure ScrapeCountQuery(db2Q, adsQ: string);
    procedure extCall;
    procedure extConnect;

  end;

var
  DM: TDM;
  AdoCon: TADOConnection;
  AdsCon: TADSConnection;
  AdoQuery: TAdoQuery;
  AdsQuery: TAdsQuery;
  LogQuery: TAdsQuery;
  stgErrorLogQuery: TAdsQuery;
  StartTS: TDateTime;
  EndTS: TDateTime;
  startID: integer;

resourcestring
  executable = 'shoretel';

implementation

constructor TDM.Create(AOwner: TComponent);
begin
  AdoCon := TADOConnection.Create(nil);
  AdsCon := TADSConnection.Create(nil);
  AdoQuery := TADOQuery.Create(nil);
  AdsQuery := TADSQuery.Create(nil);
  LogQuery := TAdsQuery.Create(nil);
  stgErrorLogQuery := TAdsQuery.Create(nil);
  AdoCon.ConnectionString := 'Driver={MySQL ODBC 5.2a Driver};Server=192.168.100.10;port = 4309;Database=shorewarecdr;User=st_cdrreport; Password=passwordcdrreport;Option=3;';;
//    AdoCon.ConnectionString := 'Provider=MSDASQL.1;Password=odbc0210;Persist Security Info=True;User ID=rydeodbc;Data Source=ArkonaSSL';
//  AdsCon.ConnectPath := '\\jon520:6363\Advantage\DDS\DDS.add';
  AdsCon.ConnectPath := '\\172.17.196.12:6363\Advantage\shoretel\shoretel.add';
//    AdsCon.ConnectPath := '\\67.135.158.12:6363\Advantage\shoretel\shoretel.add';
//  AdsCon.ConnectPath := '\\67.135.158.12:6363\DailyCut\DDS\sunday\copy\DDS.add';
  AdsCon.Username := 'adssys';
  AdsCon.Password := 'cartiva';
  AdoCon.Connected := True;
  AdsCon.Connect;
  LogQuery.AdsConnection := AdsCon;
  stgErrorLogQuery.AdsConnection := AdsCon;
end;

destructor TDM.Destroy;
begin
  AdoQuery.Close;
  FreeAndNil(AdoQuery);
  AdsQuery.Close;
  FreeAndNil(AdsQuery);
  LogQuery.Close;
  FreeAndNil(LogQuery);
  stgErrorLogQuery.Close;
  FreeAndNil(stgErrorLogQuery);
  AdsCon.Disconnect;
  FreeAndNil(adsCon);
  AdoCon.Connected := False;
  FreeAndNil(AdoCon);
  inherited;
end;


procedure TDM.extCall;
//var
//  startID: integer;
begin
try
  try
    OpenQuery(AdsQuery, 'select max(id) as startID from extCall');
    startID := AdsQuery.FieldByName('startID').AsInteger;
    CloseQuery(adsQuery);
  //  ExecuteQuery(AdsQuery, 'delete from extCallType');
    PrepareQuery(AdsQuery, ' insert into extCall(id, callId, ' +
      'sipCallId, startTime, endTime, locked,extension, durationSeconds, ' +
      'callType, workgroupCall, dialedNumber, dialedNumberRight7, ' +
      'callerID, callerIdRight7, dialedNumber10) ' +
      'values(:id, :callId, ' +
      ':sipCallId, :startTime, :endTime, :locked, :extension, :durationSeconds, ' +
      ':callType, :workgroupCall, :dialedNumber, :dialedNumberRight7, ' +
      ':callerID, :callerIdRight7, :dialedNumber10)');
    OpenQuery (AdoQuery, 'select id, callid, sipcallid, ' +
    '  starttime, endtime, locked, extension, ' +
    '  time_to_sec(time(duration)) as durationSeconds, ' +
    '  calltype, workgroupcall, ' +
    '  dialednumber, right(trim(dialedNumber), 7) as dialedNumberRight7, ' +
    '  callerid, right(trim(callerId), 7) as callerIdRight7, ' +
    '  right(trim(dialedNumber), 10) as dialedNumber10 ' +
    'from `call` ' +
    'where id >  '  + IntToStr(startID));
//    'where year(endtime) > 2000 ' +
//    '  and id > '  + IntToStr(startID));
    while not AdoQuery.Eof do
    begin
      AdsQuery.ParamByName('id').AsLargeInt := AdoQuery.FieldByName('id').AsLargeInt;
      AdsQuery.ParamByName('callID').AsInteger := AdoQuery.FieldByName('callID').AsInteger;
      AdsQuery.ParamByName('sipCallID').AsString := AdoQuery.FieldByName('sipCallID').AsString;
      AdsQuery.ParamByName('startTime').AsDateTime := AdoQuery.FieldByName('startTime').AsDateTime;
//      AdsQuery.ParamByName('endTime').AsDateTime := AdoQuery.FieldByName('endTime').AsDateTime;
//      AdsQuery.ParamByName('endTime').AsSQLTimeStamp := AdoQuery.FieldByName('endTime').AsSQLTimeStamp;
      AdsQuery.ParamByName('endTime').AsString := GetADSSqlTimeStamp(AdoQuery.FieldByName('endTime').AsDateTime);
      AdsQuery.ParamByName('locked').AsInteger := AdoQuery.FieldByName('locked').AsInteger;
      AdsQuery.ParamByName('extension').AsString := AdoQuery.FieldByName('extension').AsString;
      AdsQuery.ParamByName('durationSeconds').AsInteger := AdoQuery.FieldByName('durationSeconds').AsInteger;
      AdsQuery.ParamByName('callType').AsInteger := AdoQuery.FieldByName('callType').AsInteger;
      AdsQuery.ParamByName('workgroupCall').AsInteger := AdoQuery.FieldByName('workgroupCall').AsInteger;
      AdsQuery.ParamByName('dialedNumber').AsString := AdoQuery.FieldByName('dialedNumber').AsString;
      AdsQuery.ParamByName('dialedNumberRight7').AsString := AdoQuery.FieldByName('dialedNumberRight7').AsString;
      AdsQuery.ParamByName('callerId').AsString := AdoQuery.FieldByName('callerId').AsString;
      AdsQuery.ParamByName('callerIdRight7').AsString := AdoQuery.FieldByName('callerIdRight7').AsString;
      AdsQuery.ParamByName('dialedNumber10').AsString := AdoQuery.FieldByName('dialedNumber10').AsString;
      AdsQuery.ExecSQL;
      AdoQuery.Next;
    end;
  except
    on E: Exception do
    begin
      Raise Exception.Create('shoretel.extcall  MESSAGE: ' + E.Message);
    end;
  end;
finally
  CloseQuery(AdsQuery);
  CloseQuery(AdoQuery);
end;
end;



procedure TDM.extConnect;
//var
//  startID: integer;
var
  endId: integer;
begin
try
  try
//    OpenQuery( AdsQuery, 'SELECT max(callTableID) as startID ' +
//      'FROM extconnect a ' +
//      'WHERE EXISTS ( ' +
//      '  SELECT 1 ' +
//      '  FROM extcall ' +
//      '  WHERE id = a.callTableId)');
//        startID := AdsQuery.FieldByName('startID').AsInteger;
//        CloseQuery(adsQuery);
    OpenQuery(AdsQuery, 'select max(id) as endID from extCall');
    endID :=  AdsQuery.FieldByName('endID').AsInteger;
    CloseQuery(AdsQuery);
    PrepareQuery(AdsQuery, ' insert into extConnect(id, partyType, callTableID, ' +
    '  lineID, switchID, portNumber, portId, portName, groupId, groupName, ' +
    '  connectTime, disconnectTime, connectReason, disconnectReason, ' +
    '  partyIdFlags, partyID, partyIdName, partyIdLastName, ctrlPartyIdFlags, ' +
    '  ctrlPartyId, ctrlPartyIdName, ctrlPartyIdLastName, mailboxId, ' +
    '  talkTimeSeconds, holdTimeSeconds, ringTimeSeconds, durationSeconds) ' +
    '  values(:id, :partyType, :callTableID, ' +
    '  :lineID, :switchID, :portNumber, :portId, :portName, :groupId, :groupName, ' +
    '  :connectTime, :disconnectTime, :connectReason, :disconnectReason, ' +
    '  :partyIdFlags, :partyID, :partyIdName, :partyIdLastName, :ctrlPartyIdFlags, ' +
    '  :ctrlPartyId, :ctrlPartyIdName, :ctrlPartyIdLastName, :mailboxId, ' +
    '  :talkTimeSeconds, :holdTimeSeconds, :ringTimeSeconds, :durationSeconds)');


  OpenQuery (AdoQuery, 'select id, partytype, calltableid, lineid, ' +
  '  switchid, portnumber, portid, portname, ' +
  '  groupid, groupname, connecttime, ' +
  '  disconnecttime, connectreason, disconnectreason, ' +
  '  partyidflags, partyid, partyidname, ' +
  '  partyidlastname, ctrlpartyidflags, ctrlpartyid, ' +
  '  ctrlpartyidname, ctrlpartyidlastname, ' +
  '  mailboxid, talktimeseconds, ' +
  '  time_to_sec(time(holdtime)) as holdtimeseconds, ' +
  '  time_to_sec(time(ringtime)) as ringtimeseconds, ' +
  '  time_to_sec(time(duration)) as durationseconds ' +
  'from connect ' +
//  'where calltableid > ' + IntToStr(startID));
  'where calltableid > ' + IntToStr(startID) +
  '  and calltableid <= ' + IntToStr(endID));
//  'where year(disconnecttime) > 2000 ' +
//  '  and calltableid > '  + IntToStr(startID));




    while not AdoQuery.Eof do
    begin
      AdsQuery.ParamByName('id').AsLargeInt := AdoQuery.FieldByName('id').AsLargeInt;
      AdsQuery.ParamByName('partyType').AsInteger := AdoQuery.FieldByName('partyType').AsInteger;
      AdsQuery.ParamByName('callTableID').AsLargeInt := AdoQuery.FieldByName('callTableID').AsLargeInt;
      AdsQuery.ParamByName('lineID').AsLargeInt := AdoQuery.FieldByName('lineID').AsLargeInt;
      AdsQuery.ParamByName('switchID').AsInteger := AdoQuery.FieldByName('switchID').AsInteger;
      AdsQuery.ParamByName('portNumber').AsInteger := AdoQuery.FieldByName('portNumber').AsInteger;
      AdsQuery.ParamByName('portId').AsInteger := AdoQuery.FieldByName('portId').AsInteger;
      AdsQuery.ParamByName('portName').AsString := AdoQuery.FieldByName('portName').AsString;
      AdsQuery.ParamByName('groupId').AsInteger := AdoQuery.FieldByName('groupId').AsInteger;
      AdsQuery.ParamByName('groupName').AsString := AdoQuery.FieldByName('groupName').AsString;
      AdsQuery.ParamByName('connectTime').AsDateTime := AdoQuery.FieldByName('connectTime').AsDateTime;
      AdsQuery.ParamByName('disconnectTime').AsString := GetADSSqlTimeStamp(AdoQuery.FieldByName('disconnectTime').AsDateTime);
      AdsQuery.ParamByName('connectReason').AsInteger := AdoQuery.FieldByName('connectReason').AsInteger;
      AdsQuery.ParamByName('disconnectReason').AsInteger := AdoQuery.FieldByName('disconnectReason').AsInteger;
      AdsQuery.ParamByName('partyIdFlags').AsInteger := AdoQuery.FieldByName('partyIdFlags').AsInteger;
      AdsQuery.ParamByName('partyID').AsString := AdoQuery.FieldByName('partyID').AsString;
      AdsQuery.ParamByName('partyIdName').AsString := AdoQuery.FieldByName('partyIdName').AsString;
      AdsQuery.ParamByName('partyIdLastName').AsString := AdoQuery.FieldByName('partyIdLastName').AsString;
      AdsQuery.ParamByName('ctrlPartyIdFlags').AsInteger := AdoQuery.FieldByName('ctrlPartyIdFlags').AsInteger;
      AdsQuery.ParamByName('ctrlPartyId').AsString := AdoQuery.FieldByName('ctrlPartyId').AsString;
      AdsQuery.ParamByName('ctrlPartyIdName').AsString := AdoQuery.FieldByName('ctrlPartyIdName').AsString;
      AdsQuery.ParamByName('ctrlPartyIdLastName').AsString := AdoQuery.FieldByName('ctrlPartyIdLastName').AsString;
      AdsQuery.ParamByName('mailboxId').AsString := AdoQuery.FieldByName('mailboxId').AsString;
      AdsQuery.ParamByName('talkTimeSeconds').AsInteger := AdoQuery.FieldByName('talkTimeSeconds').AsInteger;
      AdsQuery.ParamByName('holdTimeSeconds').AsInteger := AdoQuery.FieldByName('holdTimeSeconds').AsInteger;
      AdsQuery.ParamByName('ringTimeSeconds').AsInteger := AdoQuery.FieldByName('ringTimeSeconds').AsInteger;
      AdsQuery.ParamByName('durationSeconds').AsInteger := AdoQuery.FieldByName('durationSeconds').AsInteger;
      AdsQuery.ExecSQL;
      AdoQuery.Next;
    end;


  except
    on E: Exception do
    begin
      Raise Exception.Create('shoretel.extconnect  MESSAGE: ' + E.Message);
    end;
  end;
finally
  CloseQuery(AdsQuery);
  CloseQuery(AdoQuery);
end;
end;


{$region 'Utilities'}
function TDM.GetADSSqlTimeStamp(const DateTime: TDateTime): String;
var
  ye, mo, da, ho, mi, se, ms: Word;
begin
  DecodeDateTime(DateTime, ye, mo, da, ho, mi, se, ms);
  Result := FormatWordToStr(ye, 4) + '-' +
            FormatWordToStr(mo, 2) + '-' +
            FormatWordToStr(da, 2) + ' ' +
            FormatWordToStr(ho, 2) + ':' +
            FormatWordToStr(mi, 2) + ':' +
            FormatWordToStr(se, 2);
end;

function TDM.FormatWordToStr(const inVal: Word;
  const OutLength: Integer; const PadChar: Char): String;
begin
  Result := IntToStr(inVal);
  if Length(Result) < OutLength then
    Result := StringOfChar(PadChar, OutLength - Length(Result)) + Result;
end;

procedure TDM.curLoadIsEmpty(tableName: string);
var
  curLoadCount: Integer;
begin
  OpenQuery(AdsQuery, 'select count(*) as CurLoad from curLoad' + tableName);
  curLoadCount := AdsQuery.FieldByName('CurLoad').AsInteger;
  CloseQuery(AdsQuery);
  if curLoadCount <> 0 then
    SendMail(tableName + ' failed curLoadIsEmpty');
  Assert(curLoadCount = 0, 'curLoad' + tableName + ' not empty');
end;

procedure TDM.Log(job, routine: string; jobStartTS, jobEndTS: TDateTime);
var
  sql: string;
begin
  PrepareQuery(LogQuery);
  LogQuery.SQL.Add('execute procedure etlJobLogAuditInsert(' + QuotedStr(job) + ', ' +
    QuotedStr(routine) + ', ' +  QuotedStr(GetADSSqlTimeStamp(jobStartTS)) + ', ' +
    QuotedStr(GetADSSqlTimeStamp(jobEndTS)) + ')');
  sql := LogQuery.SQL.Text;
  LogQuery.ExecSQL;
end;



procedure TDM.curLoadPrevLoad(tableName: string);
{ TODO -ojon -crefactor : is this the best way to do this? need these holding tables to be kept to a minimum size,
  don't accumlate deleted records
  12/6/11 appears to be working ok }
begin
  try
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_ZapTable(' + '''' + 'prevLoad' + tableName + '''' + ')');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + 'x' + '''' +  ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'curLoad' + tableName + '''' + ', ' + '''' + 'prevLoad' + tableName + '''' + ', 1, 0)');
      AdsQuery.Close;
      AdsQuery.AdsCloseSQLStatement();
      AdsCon.CloseCachedTables;
    ExecuteQuery(AdsQuery, 'EXECUTE PROCEDURE sp_RenameDDObject(' + '''' + 'prevLoad' + tableName + 'x' + '''' + ', ' + '''' + 'curLoad' + tableName + '''' + ', 1, 0)');
  except
    on E: Exception do
    begin
      Raise Exception.Create('curLoadPrevLoad.  MESSAGE: ' + E.Message);
    end;
  end;
end;

procedure TDM.ExecuteQuery(query: TDataSet; sql: string);
begin
//  try
    if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
    begin
      AdsQuery.AdsConnection := AdsCon;
      CloseQuery(AdsQuery);
      AdsQuery.SQL.Text := sql;
      AdsQuery.ExecSQL;
      CloseQuery(AdsQuery);
    end
    else if query.ClassParent.ClassName = 'TCustomADODataSet' then
    begin
      AdoQuery.Connection := AdoCon;
      CloseQuery(AdoQuery);
      AdoQuery.SQL.Text := sql;
      AdoQuery.ExecSQL;
      CloseQuery(AdoQuery);
    end;
//  except
//    on E: Exception do
//    begin
//      SendMail('ExecuteQuery failed - no stgErrorLog record entered');
//      exit;
//    end;
//  end;
end;

procedure TDM.OpenQuery(query: TDataSet; sql: string);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
    AdsQuery.SQL.Text := sql;
    AdsQuery.Open;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
    AdoQuery.SQL.Text := sql;
    AdoQuery.Open;
  end;
end;



procedure TDM.PrepareQuery(query: TDataSet; sql: string);
begin
//  if query = 'TAdsExtendedDataSet' then
  if query.classparent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.AdsConnection := AdsCon;
    AdsQuery.SQL.Clear;
{ TODO -ojon -cfigure out : don't remember why i commented out close - figure it out and be consistent }
//    AdsQuery.Close;
    if sql <> '' then
      AdsQuery.SQL.Text := sql;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.Connection := AdoCon;
    AdoQuery.SQL.Clear;
    if sql <> '' then
      AdoQuery.SQL.Text := sql;
  end;
end;



procedure TDM.SendMail(Subject: string; Body: string);
var
  Msg: TidMessage;
  SMTP: TIdSMTP;
begin
  Msg := TidMessage.Create;
  SMTP := TidSMTP.Create;
  Msg.From.Text := 'jandrews@cartiva.com';
  Msg.Recipients.Add.Text := 'jandrews@cartiva.com';
  Msg.Subject := Subject;
  Msg.Body.Add(Body);
  SMTP.Host := 'mail.cartiva.com';
  SMTP.AuthType := satDefault;
  SMTP.Connect;
  try
    try
      SMTP.Send(Msg);
    except
      on E: Exception do
      begin
        ExecuteQuery(stgErrorLogQuery, 'execute procedure stgErrorLogInsert (' +
            QuotedStr(GetADSSqlTimeStamp(Now)) + ', ' +
            '''' + 'send mail failed' + '''' + ', ' +
            '''' + 'no sql - line 427' + '''' + ', ' +
            QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        exit;
      end;
    end;
  finally
//    Msg.Free;
    FreeAndNil(Msg);
//    SMTP.Free;
    SMTP.Disconnect;
    FreeAndNil(SMTP);
  end;
end;


procedure TDM.CloseQuery(query: TDataSet);
begin
  if query.ClassParent.ClassName = 'TAdsExtendedDataSet' then
  begin
    AdsQuery.SQL.Clear;
    AdsQuery.Close;
  end
  else if query.ClassParent.ClassName = 'TCustomADODataSet' then
  begin
    AdoQuery.SQL.Clear;
    AdoQuery.Close;
  end;
end;

procedure TDM.ScrapeCountQuery(db2Q, adsQ: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Q);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsQ);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckQuery between ' + db2Q + '(' + IntToStr(db2Count) + ') and ' + adsQ + '(' + IntToStr(adsCount) + ')  failed');
end;

procedure TDM.ScrapeCountTable(db2Table, adsTable: string);
var
  db2Count: integer;
  adsCount: integer;
begin
  db2Table := 'rydedata.' + db2Table;
  OpenQuery(AdoQuery, 'select count(*) as db2Count from ' + db2Table);
  db2Count := AdoQuery.FieldByName('db2Count').AsInteger;
  CloseQuery(AdoQuery);
  OpenQuery(AdsQuery, 'select count(*) as adsCount from ' + adsTable);
  adsCount := AdsQuery.FieldByName('adsCount').AsInteger;
  CloseQuery(AdsQuery);
  if db2Count <> adsCount then
    Raise Exception.Create('CountCheckTable between ' + db2Table + '(' + IntToStr(db2Count) + ') and ' + adsTable + '(' + IntToStr(adsCount) + ')  failed');
end;
{$endregion}
end.


