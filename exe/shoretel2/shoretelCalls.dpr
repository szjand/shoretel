program shoretelCalls;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ActiveX,
  StrUtils,
  shoretelCallsDM in 'shoretelCallsDM.pas' {DM: TDataModule};

begin
  try
    try
      CoInitialize(nil);
      DM := TDM.Create(nil);
//      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogInsert(' +
//        QuotedStr(executable) + ', ' +
//        '''' + 'none' + '''' + ', ' +
//        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ')');
// *** active procs ***//
      DM.extCall;
      DM.extConnect;
// *** active procs ***//
//      DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
//        QuotedStr(executable) + ', ' +
//        '''' + 'none' + '''' + ', ' +
//        QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
//        'null' + ')');
    except
      on E: Exception do
      begin
//        DM.ExecuteQuery(AdsQuery, 'execute procedure zProcLogUpdate(' +
//          QuotedStr(executable) + ', ' +
//          '''' + 'none' + '''' + ', ' +
//          QuotedStr(DM.GetADSSqlTimeStamp(Now)) + ', ' +
//          QuotedStr(E.ClassName + ' ' + E.Message) + ')');
        DM.SendMail('Shoretel WTF IS GOING ON: ',   E.Message);
        exit;
      end;
    end;
  finally
    FreeAndNil(DM);
  end;
end.
